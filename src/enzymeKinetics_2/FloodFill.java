package enzymeKinetics_2;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class FloodFill {	
	/*
	Flood-fill (pixel, prev-x, prev-y):
		 1. If prev-x, prev-y == x, y -> return.
		 2. If the color of pixel is not equal to +-threshold of startPixel -> return.
		 3. Add pixel to selection area.
		 4. Perform Flood-fill (one step to the south of pixel, x, y).
		    Perform Flood-fill (one step to the north of pixel, x, y).
		    Perform Flood-fill (one step to the west of pixel, x, y).
		    Perform Flood-fill (one step to the east of pixel, x, y).
		 5. Return.
		 
	*/
	protected int threshold;
	protected Pixel startPixel;
	protected BufferedImage img;
	protected double target_r;
	protected double target_g;
	protected double target_b;
	protected ArrayList<Pixel> selection = new ArrayList<>();
	
	
	public FloodFill(BufferedImage img_, int threshold_, Pixel startPixel_) {
		img = img_;
		threshold = threshold_;
		//System.out.println("<FloodFill>  threshold " + threshold);
		startPixel = startPixel_;
		target_r = startPixel.getR();
		target_g = startPixel.getG();
		target_b = startPixel.getB();
		
	}
	
	public ArrayList<Pixel> getSelection(){
		return selection;
	}
	
	public void flood_fill(Pixel pix, int prev_x, int prev_y) {
		if(pix == null) {
			return;
		}
		
		//if this pixel is already in selection, return
		if(selection.contains(pix)) {
			return;
		}
		
		//if it is within threshold of start pixel value, add to selection. Otherwise, return
		if(within_threshold(pix)) {
			selection.add(pix);
		}else {
			return;
		}
		
		int x = (int) pix.getX();
		int y = (int) pix.getY();
		
		if(prev_x == x && prev_y == y) {
			//same as the last pixel, no need to recurse
			return;
		}
		
		//recurse on surrounding pixels
		if( !withinBounds(x, y-1)) {
			return;
		}else {
			flood_fill(new Pixel(img.getRGB(x, y-1), x, y-1), x, y);
		}
		if( ! withinBounds(x, y+1)) {
			return;
		}else {
			flood_fill(new Pixel(img.getRGB(x, y+1), x, y+1), x, y);
		}
		if(!withinBounds(x-1, y)) {
			return;
		}else {
			flood_fill(new Pixel(img.getRGB(x-1, y), x-1, y), x, y);
		}
		if( !withinBounds(x+1, y)) {
			return;
		}else {
			flood_fill(new Pixel(img.getRGB(x+1, y), x+1, y), x, y);
		}
	}
	
	private boolean withinBounds(int x, int y) {
		if (x >= 0 && y >= 0 && x < img.getWidth() && y < img.getHeight()) {
			return true;
		}
		return false;
	}
	
	private boolean within_threshold(Pixel pix) {
		if( threshold >= Math.abs(target_r - pix.getR())
				&& threshold >= Math.abs(target_g - pix.getG())
				&& threshold >= Math.abs(target_b - pix.getB()) ) {
			return true;
		}else {
			return false;
		}	
	}
	

}
