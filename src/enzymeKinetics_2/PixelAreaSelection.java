package enzymeKinetics_2;

import java.awt.image.BufferedImage;
import java.util.ArrayList;


public class PixelAreaSelection {
	
	protected ArrayList<Pixel> selection = null;
	protected int threshold;
	protected Pixel startPixel= null;
	protected BufferedImage image = null;
	protected double avg_x, avg_y;
	protected double radius;
	

	public PixelAreaSelection(BufferedImage image, int x, int y, int threshold) {
		this.image = image;
		int pixelRGB = image.getRGB(x, y);
		this.startPixel = new Pixel(pixelRGB, x, y);	
		//System.out.println("Pixel "+ startPixel);
		this.threshold = threshold;
	}
	
	private void runFloodFill(){
		FloodFill fill = new FloodFill(image, threshold, startPixel);
		//beginning coordinates do not matter so passing in (-1,-1)
		fill.flood_fill(startPixel, -1, -1);
		this.selection = fill.getSelection();
	}
	
	private void averageCoordinatesOfSelection() {
		int numberOfPixels = selection.size();
		double avg_x = 0.0;
		double avg_y = 0.0;
		
		for(Pixel p:selection) {
			avg_x += p.getX();
			avg_y += p.getY();
		}
		
		avg_x /= (numberOfPixels * 1.0);
		avg_y /= (numberOfPixels * 1.0);
		
		this.avg_x = avg_x;
		this.avg_y = avg_y;
	}
	
	private void validateSelection() {
		/* a few ways to do this...
		   one is to find max radius and shrink down 80%
		   or to find min radius and make it a perfect circle
		   we will implement a few here
		*/
		double dist;
		double max = 0.0;
		double min = Double.MAX_VALUE; 
		//set min and max radius
		for(Pixel p:selection) {
			dist = Math.sqrt( Math.pow(avg_x - p.getX(), 2) + Math.pow(avg_y - p.getY(),2));
			if(dist < min) {
				min = dist;
			}
			if(dist > max) {
				max = dist;
			}
		}
		
		System.out.println("Validating Selection size");
		System.out.println("Beginning with a selection size of " + selection.size());
		System.out.println("Max Dist: " + max + " Min Dist: "+ min);
		int countMinRemoved = 0;
		int countMaxRemoved = 0;
		
		//crop based on min 
		for(Pixel p:selection) {
			dist = Math.sqrt( Math.pow(avg_x - p.getX(), 2) + Math.pow(avg_y - p.getY(),2));
			if(dist < min) {
				countMinRemoved++;
				System.out.println("Cropping by min. Removing: " + p);
				//selection.remove(p);
			}
		}
		

		//crop based on max
		for(Pixel p:selection) {
			double max_80 = max * 0.8;
			dist = Math.sqrt( Math.pow(avg_x - p.getX(), 2) + Math.pow(avg_y - p.getY(),2));
			if(dist > max_80) {
				countMaxRemoved++;
				System.out.println("Cropping by max. Removing: " + p);
				//selection.remove(p);
			}
		}
		System.out.println();
		System.out.println("Removed " + countMinRemoved + " for Min and removed " + countMaxRemoved + " for Max.");
	}
	
	private Pixel averagePixelSelection() {
		int numberOfPixels = selection.size();
		double redAverage = 0;
		double blueAverage = 0;
		double greenAverage = 0;
		
		for(Pixel p:selection) {
			redAverage += p.getR();
			greenAverage += p.getG();
			blueAverage += p.getB();
		}
		
		//if the average is 0.0 the size may be zero which will throw and error
		//and dividing a 0.0 by anything other than zero won't change it
		if(redAverage != 0.0)
			redAverage /= (numberOfPixels * 1.0);
		if(blueAverage != 0.0)
			blueAverage /= (numberOfPixels * 1.0);
		if(greenAverage != 0.0)
			greenAverage /= (numberOfPixels * 1.0);
		
		
		Pixel averagePixel = new Pixel(redAverage, greenAverage, blueAverage, 0);
		averagePixel.setCoordinates(avg_x, avg_y);
		return averagePixel;
	}
	
	public Pixel getAveragePixelFromSelection(boolean usePixelSelection){
		if(usePixelSelection) {
			//find selection
			runFloodFill();
			
			//average selection 
			averageCoordinatesOfSelection();
			
			//validate and remove pixels if needed
			//validateSelection();
			
			//return the average pixel value
			return averagePixelSelection();
		}else {
			this.selection = new ArrayList<>();
			selection.add(startPixel);
			return startPixel;
		}
	}
	
	public ArrayList<Pixel> getSelection(){
		return selection;
	}

}
