# import the necessary packages
from picamera import PiCamera
import time
import sys

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
experimentName = "testRun_3_picture_"
try:
    # allow the camera to warmup
    print("Beginning to take pictures for " + experimentName)
    
    time.sleep(3)
    
    # experimentName = sys.argv[1]
    image = "/home/pi/Desktop/" + experimentName
        
    
    #for each second for two minutes
    for i in range(0, 720):
    
        #returns time in seconds
        start_time = time.time()
        
        camera.capture(image + str(i),'rgb')
        #camera.capture(image + str(i),'png')
        
        # Calculate elapsed time in seconds so that image taken every second
        elapsed_time = time.time() - start_time
        print(" i = " + str(i)
              + " Elapsed Time is : " + str(elapsed_time))
        if(elapsed_time > 1):
            print("had to zero out time")
            sleepTime = 0
        else:
            sleepTime = 1 - elapsed_time

        print(str(sleepTime))
        time.sleep(sleepTime)
    
    
finally:
    camera.close()
    
    
    print("Done taking pictures for " + experimentName)
    
