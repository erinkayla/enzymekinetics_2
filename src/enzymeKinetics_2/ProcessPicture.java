package enzymeKinetics_2;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;

public class ProcessPicture {
	
	protected BufferedImage image = null;	
	protected String filename;
	protected int ShiftValue;
	protected int threshold;
	protected String ColorChannel;
	protected boolean usePixelSelection;
	protected ArrayList<Double> BG1;
	protected ArrayList<Double> BG2;
	protected ArrayList<Double> BG3;
	protected ArrayList<Double> BG4;
	protected ArrayList<Double> BG5;
	protected ArrayList<Double> Sample1;
	protected ArrayList<Double> Sample2;
	protected ArrayList<Double> Sample3;
	protected ArrayList<Double> Calibration;
	protected ArrayList<PixelAreaSelection> pixelAreas;
	
	
	public ProcessPicture(String filename, String ColorChannel, int threshold, boolean usePixelSelection){
		String channel = ColorChannel;
		this.ColorChannel = ColorChannel;
		if(channel.equals("red")){
			this.ShiftValue = 16;
		}
		else if(channel.equals("green")){
			this.ShiftValue = 8;
		}
		else if(channel.equals("blue")){
			this.ShiftValue = 0;
		}
		this.filename = filename;
		this.threshold = threshold;
		this.usePixelSelection = usePixelSelection;
	}
	
	//copy image method for outputColoredSelectionImage method
	private static BufferedImage copyImage(BufferedImage source){
	    BufferedImage b = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
	    Graphics g = b.getGraphics();
	    g.drawImage(source, 0, 0, null);
	    g.dispose();
	    return b;
	}
	
	//output an image with chosen pixels colored white
	private BufferedImage outputColoredSelectionImage() {
		BufferedImage outputImage = copyImage(image);
		int r = 255;
		int g = 255;
		int b = 255; 
		for(PixelAreaSelection pas: pixelAreas) {
			for(Pixel p: pas.getSelection()) {
				int rgb = ((r&0x0ff)<<16)|((g&0x0ff)<<8)|(b&0x0ff);
				outputImage.setRGB( (int)p.getX(), (int)p.getY(), rgb);
			}
		}
		return outputImage;
	}
	
	
	public void ExtractPixels(ArrayList<Integer[]> pixels){
		
		try {
			image = ImageIO.read(new File(filename));
		} catch (IOException e){
			System.err.println("ERROR: Cannot read file: " + filename +".");
		}
		
		BG1 = new ArrayList<Double>();
		BG2 = new ArrayList<Double>();
		BG3 = new ArrayList<Double>();
		BG4 = new ArrayList<Double>();
		BG5 = new ArrayList<Double>();
		Sample1 = new ArrayList<Double>();
		Sample2 = new ArrayList<Double>();
		Sample3 = new ArrayList<Double>();
		Calibration = new ArrayList<Double>();
		pixelAreas = new ArrayList<>();
		int pixelCounter = 0;
		
		for(int row = 0; row < 8; row++){
						
			for(int col = 0; col < 9; col++){
				
				Integer[] xy = pixels.get(pixelCounter);
				int x = xy[0];
				int y = xy[1];
				
				// find selection, validate and get average pixel color value				
				PixelAreaSelection p = new PixelAreaSelection(image, x, y, threshold);
				pixelAreas.add(p);
				Pixel avg_pix = p.getAveragePixelFromSelection(usePixelSelection);  
				double colorVal = 0.0;
				
				if(ColorChannel.equals("red")){
					colorVal = avg_pix.getR();
					//System.out.println("<ProcessPicture> set red " +colorVal);
				}
				else if(ColorChannel.equals("green")){
					colorVal = avg_pix.getG();
					//System.out.println("<ProcessPicture> set green " + colorVal);
				}
				else if(ColorChannel.equals("blue")){
					colorVal = avg_pix.getB();
					//System.out.println("<ProcessPicture>  file " + filename + " at (x,y) = (" + x + "," + y+ " ) set blue " + colorVal);
				}else
					System.err.println("<ProcessPicture> SOMETHING IS WRONG WITH SETTING COLOR CHANNEL");
				

				double value = colorVal;
				
				
				
				switch(col){
					case 0: BG1.add(value); break;
					case 1: Sample1.add(value); break;
					case 2: BG2.add(value); break;
					case 3: Sample2.add(value); break;
					case 4: BG3.add(value); break;
					case 5: Sample3.add(value); break;
					case 6: BG4.add(value); break;
					case 7: Calibration.add(value); break;
					case 8: BG5.add(value); break;
					default: System.err.println("ERROR: Reached the default case. That's weird..."); break;
				}		
				
				pixelCounter++;
			}
		}		
		
		//code to output show selection on image 
		BufferedImage outputSelectionImage = outputColoredSelectionImage();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		Date date = new Date();
		String outputFile =  filename + " " + df.format(date); 
		File outImageName = new File(outputFile + "_Thres" + threshold + "_selectionColored.png");
	    try {
			ImageIO.write(outputSelectionImage, "png", outImageName);
		} catch (IOException e) {
			System.err.println("Cannot write selection colored image");
		}
	}
	
	
	protected void PrintRawData(PrintWriter pw) {
				
		pw.println("Raw Data");

		for(int row = 0; row < 8; row++){
				pw.print(BG1.get(row) + "\t");
				pw.print(Sample1.get(row) + "\t");
				pw.print(BG2.get(row) + "\t"); 
				pw.print(Sample2.get(row) + "\t"); 
				pw.print(BG3.get(row) + "\t"); 
				pw.print(Sample3.get(row) + "\t"); 
				pw.print(BG4.get(row) + "\t"); 
				pw.print(Calibration.get(row) + "\t"); 
				pw.println(BG5.get(row));
		}

		pw.println();
	}
}



