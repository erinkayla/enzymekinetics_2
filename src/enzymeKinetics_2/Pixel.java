package enzymeKinetics_2;


public class Pixel {
	
	double r, g, b, alpha;
	double x, y;
	double avg_x, avg_y;
	
	public Pixel(double r_, double g_, double b_, double alpha_) {
		r = r_;
		g = g_;
		b = b_;
		alpha = alpha_;
	}

	public Pixel(int rgb, int x_, int y_) {
		int colorValR = (rgb >> 16) & 255;
		this.r = Math.abs(colorValR - 255);
		
		int colorValG = (rgb >> 8) & 255;
		this.g = Math.abs(colorValG - 255);
		
		int colorValB = (rgb >> 0) & 255;
		this.b = Math.abs(colorValB - 255);
		
		x = x_;
		y = y_;
	}
	
	public void setCoordinates(double x_, double y_) {
		x = x_;
		y = y_;
	}
	public void setAvgCoordinates(double x_, double y_) {
		avg_x = x_;
		avg_y = y_;
	}	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getR() {
		return r;
	}
	
	public double getG() {
		return g;
	}
	
	public double getB() {
		return b;
	}
	
	public double getAlpha() {
		return alpha;
	}
	@Override
	public String toString() {
		return "Pixel at location (" + x + ", " + y + ") with RGB of (" + r +", "+ g + ", " + b + ").";
	}
	@Override
	public boolean equals(Object o){
	    if(o instanceof Pixel){
	        Pixel toCompare = (Pixel) o;
	        return (this.x == toCompare.x && this.y == toCompare.y);
	    }
	    return false;
	}
}
